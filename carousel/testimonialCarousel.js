export const testimonialCarousel = () => {
  let testTimeoutID;
  const testCarousel = document.querySelector(
    ".testimonial-comment .swiper-wrapper"
  );
  const testItems = testCarousel.querySelectorAll(
    ".testimonial-comment .swiper-slide"
  );
  const testAvataItems = document.querySelectorAll(
    ".testimonial-thumbs .swiper-slide"
  );
  const testSlides = Array.from(testItems);
  const testAvatas = Array.from(testAvataItems);
  const testButtons = testCarousel.querySelectorAll(".testCarousel__button");

  function getNextPrev() {
    const activeSlide = document.querySelector(
      ".testimonial-comment .swiper-slide--selected"
    );

    const activeIndex = testSlides.indexOf(activeSlide);

    let next, prev, nextAva, prevAva;
    if (activeIndex === testSlides.length - 1) {
      next = testSlides[0];
      nextAva = testAvatas[0];
    } else {
      next = testSlides[activeIndex + 1];
      nextAva = testAvatas[activeIndex + 1];
    }
    if (activeIndex === 0) {
      prev = testSlides[testSlides.length - 1];
      prevAva = testAvatas[testAvatas.length - 1];
    } else {
      prev = testSlides[activeIndex - 1];
      prevAva = testAvatas[activeIndex - 1];
    }
    return [next, prev, nextAva, prevAva];
  }
  function getPosition() {
    const activeSlide = document.querySelector(
      ".testimonial-comment .swiper-slide--selected"
    );
    const activeIndex = testSlides.indexOf(activeSlide);
    const [next, prev, nextAva, prevAva] = getNextPrev();
    testSlides.forEach((testSlide, index) => {
      if (index === activeIndex) {
        testSlide.style.transform = "translateX(0)";
      } else if (testSlide === next) {
        testSlide.style.transform = "translateX(100%)";
      } else if (testSlide === prev || index < activeIndex) {
        testSlide.style.transform = "translateX(-100%)";
      } else {
        testSlide.style.transform = "translateX(100%)";
      }
      testSlide.addEventListener("transitionend", () => {
        testSlide.classList.remove("top");
      });
    });
    testAvatas.forEach((testAvataSlide, index) => {
      if (index === activeIndex) {
        testAvataSlide.style.transform = "translateX(0)";
      } else if (testAvataSlide === nextAva) {
        testAvataSlide.style.transform = "translateX(100%)";
      } else if (testAvataSlide === prevAva || index < activeIndex) {
        testAvataSlide.style.transform = "translateX(-100%)";
      } else {
        testAvataSlide.style.transform = "translateX(100%)";
      }
      testAvataSlide.addEventListener("transitionend", () => {
        testAvataSlide.classList.remove("top");
      });
    });
  }

  const getNextSlide = () => {
    clearInterval(testTimeoutID);
    const currentSlide = document.querySelector(
      ".testimonial-comment .swiper-slide--selected"
    );

    const currentAva = document.querySelector(
      ".testimonial-thumbs .swiper-slide--selected"
    );
    const [next, prev, nextAva, prevAva] = getNextPrev();

    testSlides.forEach((slide) => {
      slide.classList.remove("swiper-slide--selected");
    });
    testAvatas.forEach((slide) => {
      slide.classList.remove("swiper-slide--selected");
    });
    prevAva.style.transform = "translateX(-200%)";

    currentSlide.classList.add("top");
    next.classList.add("top");
    currentAva.classList.add("top");
    nextAva.classList.add("top");
    currentSlide.style.transform = "translate(-100%)";
    currentAva.style.transform = "translate(-100%)";
    currentSlide.classList.remove("swiper-slide--selected");
    currentAva.classList.remove("swiper-slide--selected");
    next.style.transform = "translateX(0)";
    next.classList.add("swiper-slide--selected");
    nextAva.style.transform = "translateX(0)";
    nextAva.classList.add("swiper-slide--selected");
    getPosition();

    autoLoop();
    getActiveDot();
  };
  const getPrevSlide = () => {
    clearInterval(testTimeoutID);
    const currentSlide = document.querySelector(
      ".testimonial-comment .swiper-slide--selected"
    );
    const currentAva = document.querySelector(
      ".testimonial-thumbs .swiper-slide--selected"
    );
    const [next, prev, nextAva, prevAva] = getNextPrev();

    testSlides.forEach((slide) => {
      slide.classList.remove("swiper-slide--selected");
    });
    testAvatas.forEach((slide) => {
      slide.classList.remove("swiper-slide--selected");
    });

    currentSlide.classList.add("top");
    prev.classList.add("top");
    currentAva.classList.add("top");
    prevAva.classList.add("top");
    currentSlide.style.transform = "translate(-100%)";
    currentAva.style.transform = "translate(-100%)";
    currentSlide.classList.remove("swiper-slide--selected");
    currentAva.classList.remove("swiper-slide--selected");
    prev.style.transform = "translateX(0)";
    prev.classList.add("swiper-slide--selected");
    prevAva.style.transform = "translateX(0)";
    prevAva.classList.add("swiper-slide--selected");
    getPosition();
    autoLoop();
    getActiveDot();
  };

  function getActiveDot() {
    testButtons.forEach((button) => {
      button.classList.remove("testCarousel__button--selected");
    });
    const activeSlide = document.querySelector(
      ".testimonial-comment .swiper-slide--selected"
    );
    const activeIndex = testSlides.indexOf(activeSlide);
    testButtons[activeIndex].classList.add("testCarousel__button--selected");
  }
  function functionalDots() {
    testButtons.forEach((button, index) => {
      button.addEventListener("click", (e) => {
        getButtonSlide(index);
        e.stopPropagation();
      });
    });
  }
  function getButtonSlide(index) {
    clearInterval(testTimeoutID);
    const currentSlide = document.querySelector(
      ".testimonial-comment .swiper-slide--selected"
    );
    const currentAva = document.querySelector(
      ".testimonial-thumbs .swiper-slide--selected"
    );

    const activeIndex = testSlides.indexOf(currentSlide);
    const targetSlide = testSlides[index];
    const targetAva = testAvatas[index];
    testSlides.forEach((slide) => {
      slide.classList.remove("swiper-slide--selected");
    });
    testAvatas.forEach((slide) => {
      slide.classList.remove("swiper-slide--selected");
    });

    currentSlide.classList.add("top");
    targetSlide.classList.add("top");
    currentAva.classList.add("top");
    targetAva.classList.add("top");
    if (index > activeIndex) {
      currentSlide.style.transform = "translate(-100%)";
      currentAva.style.transform = "translate(-100%)";
    } else if (index < activeIndex) {
      currentSlide.style.transform = "translate(100%)";
      currentAva.style.transform = "translate(100%)";
    }
    currentSlide.classList.remove("swiper-slide--selected");
    currentAva.classList.remove("swiper-slide--selected");
    targetSlide.style.transform = "translateX(0)";
    targetSlide.classList.add("swiper-slide--selected");
    targetAva.style.transform = "translateX(0)";
    targetAva.classList.add("swiper-slide--selected");
    getPosition();
    autoLoop();
    getActiveDot();
  }
  function functionalAvatars() {
    testAvataItems.forEach((avata, index) => {
      avata.addEventListener("click", (e) => {
        getAvatarSlide(index);
        e.stopPropagation();
      });
    });
  }
  function getAvatarSlide(index) {
    clearInterval(testTimeoutID);
    const currentSlide = document.querySelector(
      ".testimonial-comment .swiper-slide--selected"
    );
    const currentAva = document.querySelector(
      ".testimonial-thumbs .swiper-slide--selected"
    );

    const activeIndex = testSlides.indexOf(currentSlide);
    const targetSlide = testSlides[index];
    const targetAva = testAvatas[index];
    testSlides.forEach((slide) => {
      slide.classList.remove("swiper-slide--selected");
    });
    testAvatas.forEach((slide) => {
      slide.classList.remove("swiper-slide--selected");
    });

    currentSlide.classList.add("top");
    targetSlide.classList.add("top");
    currentAva.classList.add("top");
    targetAva.classList.add("top");
    if (index > activeIndex) {
      currentSlide.style.transform = "translate(-100%)";
      currentAva.style.transform = "translate(-100%)";
    } else if (index < activeIndex) {
      currentSlide.style.transform = "translate(100%)";
      currentAva.style.transform = "translate(100%)";
    }
    currentSlide.classList.remove("swiper-slide--selected");
    currentAva.classList.remove("swiper-slide--selected");
    targetSlide.style.transform = "translateX(0)";
    targetSlide.classList.add("swiper-slide--selected");
    targetAva.style.transform = "translateX(0)";
    targetAva.classList.add("swiper-slide--selected");
    getPosition();
    autoLoop();
    getActiveDot();
  }

  function autoLoop() {
    testTimeoutID = setInterval(() => {
      getNextSlide();
    }, 3000);
  }

  const dragCarouselSlider = () => {
    let swipers = document.querySelector(".testimonial-comment");
    let innerSlider = document.querySelector(
      ".testimonial-comment .swiper-slide--selected"
    );
    let pressed = false;
    let startx, endx;

    swipers.addEventListener("mousedown", (e) => {
      pressed = true;
      startx = e.offsetX - innerSlider.offsetLeft;
      swipers.style.cursor = "grabbing";
    });
    swipers.addEventListener("mouseenter", () => {
      swipers.style.cursor = "grab";
    });
    swipers.addEventListener("mouseup", () => {
      swipers.style.cursor = "grab";
    });

    swipers.addEventListener("mousemove", (e) => {
      if (!pressed) return;
      e.preventDefault();
      endx = e.offsetX;
    });
    swipers.addEventListener("mouseup", () => {
      pressed = false;
      if (endx < startx) {
        getNextSlide();
      } else if (endx > startx) {
        getPrevSlide();
      }
    });
  };
  dragCarouselSlider();
  getPosition();
  autoLoop();
  functionalDots();
  functionalAvatars();
};
