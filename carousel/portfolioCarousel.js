export const handleCarousel = (carousel) => {
  let timeoutId;
  const items = carousel.querySelectorAll(".carousel__item");
  const slides = Array.from(items);

  const buttons = carousel.querySelectorAll(".carousel__button");
  function getNextPrev() {
    const activeSlide = document.querySelector(".carousel__item--selected");
    const activeIndex = slides.indexOf(activeSlide);

    let next, prev;
    if (activeIndex === slides.length - 1) {
      next = slides[0];
    } else {
      next = slides[activeIndex + 1];
    }
    if (activeIndex === 0) {
      prev = slides[slides.length - 1];
    } else {
      prev = slides[activeIndex - 1];
    }

    return [next, prev];
  }
  function getPosition() {
    const activeSlide = document.querySelector(".carousel__item--selected");
    const activeIndex = slides.indexOf(activeSlide);

    const [next, prev] = getNextPrev();
    slides.forEach((slide, index) => {
      if (index === activeIndex) {
        slide.style.transform = "translateX(0)";
      } else if (slide === next) {
        slide.style.transform = "translateX(100%)";
      } else if (slide === prev || index < activeIndex) {
        slide.style.transform = "translateX(-100%)";
      } else {
        slide.style.transform = "translateX(100%)";
      }
      slide.addEventListener("transitionend", () => {
        slide.classList.remove("top");
      });
    });
  }
  const prevButton = document.querySelector(".carousel__button--prev");
  const nextButton = document.querySelector(".carousel__button--next");
  nextButton.addEventListener("click", (event) => {
    getNextSlide();
    event.stopPropagation();
  });
  prevButton.addEventListener("click", (event) => {
    getPrevSlide();
    event.stopPropagation();
  });

  const getNextSlide = () => {
    const current = document.querySelector(".carousel__item--selected");
    const [next, prev] = getNextPrev();

    current.classList.add("top");
    next.classList.add("top");
    current.style.transform = "translate(-100%)";
    current.classList.remove("carousel__item--selected");
    next.style.transform = "translateX(0)";
    next.classList.add("carousel__item--selected");
    getPosition();
    getActiveDot();
  };

  function getPrevSlide() {
    const current = document.querySelector(".carousel__item--selected");
    const [next, prev] = getNextPrev();

    current.classList.add("top");
    prev.classList.add("top");
    current.style.transform = "translate(100%)";
    current.classList.remove("carousel__item--selected");
    prev.style.transform = "translateX(0)";
    prev.classList.add("carousel__item--selected");
    getPosition();
    getActiveDot();
  }
  function getActiveDot() {
    buttons.forEach((button) => {
      button.classList.remove("carousel__button--selected");
    });
    const activeSlide = document.querySelector(".carousel__item--selected");
    const activeIndex = slides.indexOf(activeSlide);
    buttons[activeIndex].classList.add("carousel__button--selected");
  }
  function functionalDots() {
    buttons.forEach((button, index) => {
      button.addEventListener("click", (e) => {
        getButtonSlide(index);
        e.stopPropagation();
      });
    });
  }
  function getButtonSlide(index) {
    const current = document.querySelector(".carousel__item--selected");
    const activeIndex = slides.indexOf(current);
    const targetItem = slides[index];
    slides.forEach((slide) => {
      slide.classList.remove("carousel__item--selected");
    });

    current.classList.add("top");
    targetItem.classList.add("top");
    if (index > activeIndex) {
      current.style.transform = "translate(-100%)";
    } else if (index < activeIndex) {
      current.style.transform = "translate(100%)";
    }
    current.classList.remove("carousel__item--selected");
    targetItem.style.transform = "translateX(0)";
    targetItem.classList.add("carousel__item--selected");
    getPosition();

    getActiveDot();
  }

  function autoLoop() {
    timeoutId = setInterval(() => {
      getNextSlide();
    }, 3000);
  }
  document.querySelector(".fa-play-circle").addEventListener("click", (e) => {
    document.querySelector(".fa-play-circle").style.display = "none";
    document.querySelector(".fa-pause-circle").style.display = "inline-block";
    autoLoop();
    e.stopPropagation();
  });
  document.querySelector(".fa-pause-circle").addEventListener("click", (e) => {
    document.querySelector(".fa-pause-circle").style.display = "none";
    document.querySelector(".fa-play-circle").style.display = "inline-block";
    clearTimeout(timeoutId);
    e.stopPropagation();
  });

  function openFullscreen() {
    if (carousel.requestFullscreen) {
      carousel.requestFullscreen();
    } else if (carousel.webkitRequestFullscreen) {
      /* Safari */
      carousel.webkitRequestFullscreen();
    } else if (carousel.msRequestFullscreen) {
      /* IE11 */
      carousel.msRequestFullscreen();
    }
  }
  function closeFullscreen() {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
      /* Safari */
      document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
      /* IE11 */
      document.msExitFullscreen();
    }
  }
  const fullScreen = document.querySelector(".go_fullscreen");

  fullScreen.addEventListener("click", (e) => {
    if (fullScreen.classList.contains("go_fullscreen")) {
      fullScreen.classList.add("exit_fullscreen");
      fullScreen.classList.remove("go_fullscreen");
      openFullscreen();
      e.stopPropagation();
    } else if (fullScreen.classList.contains("exit_fullscreen")) {
      fullScreen.classList.remove("exit_fullscreen");
      fullScreen.classList.add("go_fullscreen");
      closeFullscreen();
      e.stopPropagation();
    }
  });

  const dragCarouselSlider = () => {
    let swipers = document.querySelector(".carousel_wrap");
    let innerSlider = document.querySelector(".carousel__item--selected");
    let pressed = false;
    let startx, endx;
    swipers.addEventListener("mousedown", (e) => {
      pressed = true;
      startx = e.offsetX - innerSlider.offsetLeft;
      swipers.style.cursor = "grabbing";
      e.stopPropagation();
    });
    swipers.addEventListener("mouseenter", () => {
      swipers.style.cursor = "grab";
    });
    swipers.addEventListener("mouseup", () => {
      swipers.style.cursor = "grab";
    });

    swipers.addEventListener("mousemove", (e) => {
      if (!pressed) return;
      e.preventDefault();
      endx = e.offsetX;
    });
    swipers.addEventListener("mouseup", () => {
      pressed = false;
      if (endx < startx) {
        getNextSlide();
      } else if (endx > startx) {
        getPrevSlide();
      }
    });
  };
  dragCarouselSlider();
  getPosition();
  getActiveDot();
  functionalDots();
};
