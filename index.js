import { blogCarousel } from "./carousel/blogCarousel.js";
import { handleCarousel } from "./carousel/portfolioCarousel.js";
import { testimonialCarousel } from "./carousel/testimonialCarousel.js";

let isCounted = false;
const showContent = () => {
  let introduceElement = document.querySelector(".introduce");
  if (
    introduceElement.getBoundingClientRect().top > -500 &&
    introduceElement.getBoundingClientRect().top < window.innerHeight
  ) {
    introduceElement.classList.add("active");
  }
  let aboutUsElement = document.getElementById("aboutus");
  if (
    aboutUsElement.getBoundingClientRect().top > -500 &&
    aboutUsElement.getBoundingClientRect().top < window.innerHeight
  ) {
    document
      .querySelector(".aboutus__img")
      .classList.add("aboutus_move_to_right");
    document
      .querySelector(".aboutus__detail")
      .classList.add("aboutus_move_to_left");
  }

  let numberElement = document.getElementById("number");

  if (
    !isCounted &&
    numberElement.getBoundingClientRect().top < window.innerHeight &&
    numberElement.getBoundingClientRect().top > -200
  ) {
    const countes = document.querySelectorAll(".counter");
    countes.forEach((counter) => {
      counter.innerText = "0";
      const updateCounter = () => {
        const target = +counter.getAttribute("data-target");
        const current = +counter.innerText;
        const increment = target / 20;
        if (current < target) {
          counter.innerText = `${Math.ceil(current + increment)}`;
          setTimeout(updateCounter, 50);
        } else {
          counter.innerText = target;
        }
      };
      updateCounter();
    });
    isCounted = true;
  }
  let featuresElement = document.getElementById("features");
  if (
    featuresElement.getBoundingClientRect().top > -1200 &&
    featuresElement.getBoundingClientRect().top < window.innerHeight
  ) {
    document.querySelector(".features__title").classList.add("move_right");
    let featureContainers = document.querySelectorAll(".ft_items__container");
    for (const container of featureContainers) {
      container.classList.add("moveleft");
    }
  }
  let portfolioElement = document.getElementById("portfolio");

  if (
    portfolioElement.getBoundingClientRect().top > -1000 &&
    portfolioElement.getBoundingClientRect().top < window.innerHeight
  ) {
    let portfolioItems = document.querySelectorAll(".portfolio__items");
    for (const portfolioItem of portfolioItems) {
      portfolioItem.classList.add("moveup");
    }
  }
  let subscribeElement = document.querySelector(".subscribe");
  if (subscribeElement.getBoundingClientRect().top < window.innerHeight) {
    document.querySelector(".subscribe__container").classList.add("active");
  }
};
showContent();

window.addEventListener("scroll", function () {
  showContent();
});

// Carousel porfolio
const portfolioCarousel = document.querySelector(".portfolio__carousel");
const items = portfolioCarousel.querySelectorAll(".carousel__item");
let navHTML = "";
items.forEach((item) => {
  let contentDiv = `<div class="carousel__button">${item.innerHTML}</div>`;
  navHTML += contentDiv;
});

portfolioCarousel.insertAdjacentHTML(
  "beforeend",
  `
       <div class="carousel__nav">
       ${navHTML}
       </div>
       `
);
const showCarouselIcons = document.querySelectorAll(".overlay_toleft");
const buttons = portfolioCarousel.querySelectorAll(".carousel__button");
const carouselNav = document.querySelector(".carousel__nav");
showCarouselIcons.forEach((icon, index) => {
  icon.addEventListener("click", () => {
    let currentIndex = index;
    if (carouselNav.classList.contains("hide_pannel")) {
      carouselNav.classList.remove("hide_pannel");
    }
    // show carousel
    portfolioCarousel.classList.add("portfolio__carousel--active");
    // hide carousel when click somewhere except items and buttons
    portfolioCarousel.addEventListener("click", () => {
      portfolioCarousel.classList.remove("portfolio__carousel--active");
      document.body.style.overflowY = "scroll";

      unSelectItem();
    });
    // close modal button
    document
      .querySelector(".portfolio__carousel .fa-times")
      .addEventListener("mousedown", () => {
        portfolioCarousel.classList.remove("portfolio__carousel--active");
        document.body.style.overflowY = "scroll";
        unSelectItem();
      });
    // close carousel pannel
    document.querySelector(".fa-eye").addEventListener("click", (event) => {
      carouselNav.classList.toggle("hide_pannel");
      event.stopPropagation();
    });
    // prevent scroll when popup modal
    document.body.style.overflowY = "hidden";

    items.forEach((item) => {
      item.addEventListener("click", (event) => {
        event.stopPropagation();
      });
    });
    // unselect items
    const unSelectItem = () => {
      items.forEach((item) => {
        item.classList.remove("carousel__item--selected");
      });
      buttons.forEach((button) => {
        button.classList.remove("carousel__button--selected");
      });
    };
    // select the initial selected item
    items[currentIndex].classList.add("carousel__item--selected");
    buttons[currentIndex].classList.add("carousel__button--selected");
    handleCarousel(portfolioCarousel);
  });
});

// Testimonial carousel
testimonialCarousel();
blogCarousel();
